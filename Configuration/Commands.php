<?php
return [
    'commands:cssclassusage' => [
        'class' => \CodingMs\Commands\Command\CssClassUsageCommand::class,
        'schedulable' => true,
    ],
    'commands:database' => [
        'class' => \CodingMs\Commands\Command\DatabaseCommand::class,
        'schedulable' => false,
    ],
    'commands:filesystem' => [
        'class' => \CodingMs\Commands\Command\FileSystemCommand::class,
        'schedulable' => true,
    ],
    'commands:generateSlugs' => [
        'class' => \CodingMs\Commands\Command\GenerateSlugsCommand::class,
        'schedulable' => false,
    ],
    'commands:image' => [
        'class' => \CodingMs\Commands\Command\ImageCommand::class,
        'schedulable' => true,
    ],
    'commands:imageextension' => [
        'class' => \CodingMs\Commands\Command\ImageExtensionFixCommand::class,
        'schedulable' => true,
    ],
    'commands:imagertefix' => [
        'class' => \CodingMs\Commands\Command\ImageRteFixCommand::class,
        'schedulable' => true,
    ],
    'commands:notifyusers' => [
        'class' => \CodingMs\Commands\Command\NotifyUsersCommand::class,
        'schedulable' => true,
    ],
    'commands:record' => [
        'class' => \CodingMs\Commands\Command\RecordCommand::class,
        'schedulable' => true,
    ],
    'commands:resizeImages' => [
        'class' => \CodingMs\Commands\Command\ResizeImagesCommand::class,
    ],
    'commands:translation' => [
        'class' => \CodingMs\Commands\Command\TranslationCommand::class,
        'schedulable' => true,
    ],
];
