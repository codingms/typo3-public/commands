<?php

declare(strict_types=1);

namespace CodingMs\Commands\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2021 Christian Bülter <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Psr\Log\LogLevel as LogLevelBaseClass;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class LogUtility
 *
 * Shows messages as flash message and in the console and writes a log file.
 *
 * Usage example from a symfony command:
 *
 * $this->io = new SymfonyStyle($input, $output);
 * $this->logUtility = GeneralUtility::makeInstance(LogUtility::class, __CLASS__, $this->io);
 * $this->logUtility->logAndNotify('This is the way!');
 *
 * Usage example from other places (without console output):
 *
 * $this->logUtility = GeneralUtility::makeInstance(LogUtility::class, __CLASS__);
 * $this->logUtility->logAndNotify('This is the way!');
 *
 * Or if you want to log an error:
 *
 * $this->logUtility->logAndNotify('This is the way!', LogUtility::ERROR);
 */
class LogUtility
{
    const NOTICE = -2;
    const INFO = -1;
    const OK = 0;
    const WARNING = 1;
    const ERROR = 2;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var LogManager
     */
    protected $logManager;

    /**
     * @var SymfonyStyle|null
     */
    protected $io;

    /**
     * SendMailUtility constructor.
     */
    /**
     * LogUtility constructor.
     * @param string $class class name
     * @param SymfonyStyle|null $io instance of SymfonyStyle or null
     */
    public function __construct(string $class, SymfonyStyle $io=null)
    {
        // We cannot use Dependency Injection here because we want to support TYPO3 9
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->logManager = GeneralUtility::makeInstance(LogManager::class);
        $this->logger = $this->logManager->getLogger($class);
        $this->io = $io;
    }

    /**
     * Logs and shows messages.
     * Creates a flash massage if a backend user is logged in.
     * Writes to the console if available.
     * Logs using the TYPO3 core logging system (in our case logs to a file in var/log/).
     *
     * @param string $message Message to log
     * @param int $severity One of the following constants of this static constants of this class NOTICE, INFO, OK, WARNING, ERROR
     * @param string $title Title, only for flash messages
     * @param array<String> $data Additional data, only for file logging
     * @throws AspectNotFoundException
     * @throws Exception
     */
    public function logAndNotify(string $message, int $severity = self::INFO, string $title = '', array $data = []): void
    {
        /** @var Context $context */
        $context = GeneralUtility::makeInstance(Context::class);
        $backendUserIsLoggedIn = $context->getPropertyFromAspect('backend.user', 'isLoggedIn');

        // Map severity to FlashMessage and Logger severities and console styles
        switch ($severity) {
            case self::ERROR:
                $flashMessageSeverity = AbstractMessage::ERROR;
                $logLevel = LogLevelBaseClass::ERROR;
                $consoleStyling = ['<error>', '</error>'];
                break;
            case self::WARNING:
                $flashMessageSeverity = AbstractMessage::WARNING;
                $logLevel = LogLevel::WARNING;
                $consoleStyling = ['<comment>', '</comment>'];
                break;
            case self::OK:
                $flashMessageSeverity = AbstractMessage::OK;
                $logLevel = LogLevelBaseClass::INFO;
                $consoleStyling = ['<info>', '</info>'];
                break;
            case self::INFO:
                $flashMessageSeverity = AbstractMessage::INFO;
                $logLevel = LogLevelBaseClass::INFO;
                $consoleStyling = ['', ''];
                break;
            default:
            case 'NOTICE':
                $flashMessageSeverity = AbstractMessage::NOTICE;
                $logLevel = LogLevelBaseClass::NOTICE;
                $consoleStyling = ['', ''];
                break;
        }

        if ($backendUserIsLoggedIn) {
            FlashMessageUtility::message($message, $title, $flashMessageSeverity);
        }

        if ($this->io instanceof SymfonyStyle) {
            $this->io->writeln($consoleStyling[0] . $message . $consoleStyling[1]);
        }

        $this->log($message, $logLevel, $data);
    }

    /**
     * @param string $message
     * @param string $logLevel Use log levels from \Psr\Log\LogLevel
     * @param mixed[] $data
     * @noinspection PhpPluralMixedCanBeReplacedWithArrayInspection
     */
    public function log(string $message, string $logLevel = LogLevelBaseClass::NOTICE, array $data = []): void
    {
        $this->logger->log($logLevel, $message, $data);
    }
}
