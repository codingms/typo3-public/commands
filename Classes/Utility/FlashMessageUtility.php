<?php

declare(strict_types=1);

namespace CodingMs\Commands\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2021 Christian Bülter <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Flash message Tools
 */
class FlashMessageUtility
{
    /**
     * Simple wrapper to for creating flash messages
     *
     * @param string $message
     * @param string $title
     * @param int $status
     * @throws Exception
     */
    public static function message(string $message, string $title, int $status): void
    {
        /** @var Context $context */
        $context = GeneralUtility::makeInstance(Context::class);
        $backendUserIsLoggedIn = $context->getPropertyFromAspect('backend.user', 'isLoggedIn');

        if ($backendUserIsLoggedIn) {
            /** @var FlashMessageService $flashMessageService */
            $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
            $messageQueue = $flashMessageService->getMessageQueueByIdentifier();
            /** @var FlashMessage $flashMessageInfo */
            $flashMessageInfo = GeneralUtility::makeInstance(
                FlashMessage::class,
                $message,
                $title,
                $status,
                true
            );
            $messageQueue->enqueue($flashMessageInfo);
        }
    }
}
