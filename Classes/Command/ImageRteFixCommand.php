<?php

namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Exception;

/**
 * Image RTE fix commands
 *
 * @package commands
 *
 */
class ImageRteFixCommand extends DefaultCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Image RTE fix commands from the command line.');
    }

    /**
     * Fixes migrated images of the RTE.
     *
     * Finds all images where content type and image file extension is not the same and fix the content.
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \TYPO3\CMS\Core\Exception
     */

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executionStartTime = microtime(true);
        //
        //
        $imagesNotFound = 0;
        $processedFiles = 0;
        $targetPath = GeneralUtility::getFileAbsFileName('fileadmin/_migrated/RTE/');
        $sourcePath = GeneralUtility::getFileAbsFileName('uploads/');
        // Catch all exceptions!
        try {
            // initialize command
            if (!$this->initializeFileLogging()) {
                return 1;
            }
            $migratedFiles = GeneralUtility::getFilesInDir($targetPath);
            foreach ($migratedFiles as $migratedFile) {
                $targetFile = $targetPath . $migratedFile;
                $sourceFile = $sourcePath . $migratedFile;
                //
                // Check duplicate file extension and remove them
                $targetFileExtension = pathinfo($targetFile, PATHINFO_EXTENSION);
                $duplicateFileExtension = '.' . $targetFileExtension . '.' . $targetFileExtension;
                if (substr($targetFile, strlen($duplicateFileExtension) * -1, strlen($duplicateFileExtension)) === $duplicateFileExtension) {
                    $sourceFile = substr($sourceFile, 0, (strlen($targetFileExtension) + 1) * -1);
                }
                if (!file_exists($sourceFile)) {
                    $sourceFile = str_replace('RTEmagicC', 'RTEmagicP', $sourceFile);
                }
                if (file_exists($sourceFile)) {
                    copy($sourceFile, $targetFile);
                    $processedFiles++;
                } else {
                    $imagesNotFound++;
                    $this->log('File: ' . $sourceFile, 'error');
                }
            }
        } catch (Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
            $this->log('Exception-Code: ' . $e->getCode(), 'error');
            $this->log('Exception-Line: ' . $e->getLine(), 'error');
            $this->log('Exception-Trace: ' . $e->getTraceAsString(), 'error');
        }
        $executionEndTime = microtime(true);
        $executionTime = $executionEndTime - $this->executionStartTime;
        $this->log('Migrated images fixed: ' . $processedFiles . ', images not found: ' . $imagesNotFound, 'info');
        $this->log('Execution time: ' . $executionTime, 'info');

        return 0;
    }
}
