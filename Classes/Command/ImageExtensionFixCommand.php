<?php

namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Exception;
use SplFileInfo;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

/**
 * Image extension fix commands
 *
 * @package commands
 *
 */
class ImageExtensionFixCommand extends DefaultCommand
{
    /**
     * @var array
     */
    protected array $folder = [];

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Image extension commands from the command line.');
        $this->addArgument('identifier', InputArgument::REQUIRED, 'Folder identifier, for example 1:/user_upload/');
    }

    /**
     * Fixes image extensions from folder identifier
     *
     * Finds all images where content type and image file extension is not the same and fix the content.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $identifier = $input->getArgument('identifier');
        $this->executionStartTime = microtime(true);
        $imagesChecked = 0;
        $imagesInvalid = 0;
        $validExtensions = ['gif', 'jpg', 'jpeg', 'png', 'bmp', 'tiff'];
        // Catch all exceptions!
        try {
            // initialize command
            if (!$this->initializeFileLogging()) {
                return 1;
            }
            //
            // Explode folder identifier
            $identifierParts = explode(':', $identifier);
            if (count($identifierParts) !== 2) {
                $this->log('Please enter a valid folder identifier!', 'error');
                return 1;
            }
            //
            // Fetch storage
            /** @var StorageRepository $storageRepository */
            $storageRepository = $this->objectManager->get(StorageRepository::class);
            $storage = $storageRepository->findByUid((int)$identifierParts[0]);
            if (!($storage instanceof ResourceStorage)) {
                $this->log('Storage not found!', 'error');
                return 1;
            }
            $this->log('Working on storage: ' . $storage->getName(), 'info');
            //
            // Fetch folder
            /** @var Folder $folder */
            $folder = $storage->getFolder($identifierParts[1]);
            $folderUrl = (string)$storage->getPublicUrl($folder);
            if (substr($folderUrl, 0, 1) === '/') {
                $folderUrl = substr($folderUrl, 1);
            }
            $absFolder = GeneralUtility::getFileAbsFileName($folderUrl);
            $this->log('Working in folder storage: ' . $folder->getName() . ' (' . $absFolder . ')', 'info');
            /**
             * @var string $name
             * @var SplFileInfo $object
             */
            $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($absFolder), RecursiveIteratorIterator::SELF_FIRST);
            foreach ($objects as $name => $object) {
                if ($object->isFile()) {
                    $fileExtension = trim(strtolower($object->getExtension()));
                    if (in_array($fileExtension, $validExtensions)) {
                        $imagesChecked++;
                        $imageFile = $object->getRealPath();
                        //
                        // Fetch image, but only when it isn't valid!
                        $image = $this->fetchImage($imageFile, $fileExtension);
                        //
                        // Write image back
                        if ($image !== null) {
                            $imagesInvalid++;
                            $this->rewriteImage($image, $imageFile, $fileExtension);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
            $this->log('Exception-Code: ' . $e->getCode(), 'error');
            $this->log('Exception-Line: ' . $e->getLine(), 'error');
            $this->log('Exception-Trace: ' . $e->getTraceAsString(), 'error');
        }
        $executionEndTime = microtime(true);
        $executionTime = $executionEndTime - $this->executionStartTime;
        $this->log('Images checked: ' . $imagesChecked . ', images invalid: ' . $imagesInvalid, 'info');
        $this->log('Execution time: ' . $executionTime, 'info');

        return 0;
    }

    /**
     * @param $imageFile
     * @param $fileExtension
     * @return false|resource|null
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function fetchImage($imageFile, $fileExtension)
    {
        $image = null;
        $imageInfo = getimagesize($imageFile);
        if ($imageInfo[2] == IMAGETYPE_GIF && $fileExtension !== 'gif') {
            $this->log('    Found type:gif but is ' . $fileExtension . ' (' . $imageFile . ')', 'warning');
            $image = imagecreatefromgif($imageFile);
        }
        if ($imageInfo[2] == IMAGETYPE_JPEG && $fileExtension !== 'jpg' && $fileExtension !== 'jpeg') {
            $this->log('    Found type:jpeg but is ' . $fileExtension . ' (' . $imageFile . ')', 'warning');
            $image = imagecreatefromjpeg($imageFile);
        }
        if ($imageInfo[2] == IMAGETYPE_PNG && $fileExtension !== 'png') {
            $this->log('    Found type:png but is ' . $fileExtension . ' (' . $imageFile . ')', 'warning');
            $image = imagecreatefrompng($imageFile);
        }
        if ($imageInfo[2] == IMAGETYPE_BMP && $fileExtension !== 'bmp') {
            $this->log('    Found type:bmp but is ' . $fileExtension . ' (' . $imageFile . ')', 'warning');
            $image = imagecreatefrombmp($imageFile);
        }
        return $image;
    }

    /**
     * @param $image
     * @param $imageFile
     * @param $fileExtension
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function rewriteImage($image, $imageFile, $fileExtension): void
    {
        switch ($fileExtension) {
            case 'gif':
                imagegif($image, $imageFile);
                imagedestroy($image);
                $this->log('  Fix image: ' . $imageFile, 'ok');
                break;
            case 'jpg':
                imagejpeg($image, $imageFile);
                imagedestroy($image);
                $this->log('  Fix image: ' . $imageFile, 'ok');
                break;
            case 'jpeg':
                imagejpeg($image, $imageFile);
                imagedestroy($image);
                $this->log('  Fix image: ' . $imageFile, 'ok');
                break;
            case 'png':
                imagepng($image, $imageFile);
                imagedestroy($image);
                $this->log('  Fix image: ' . $imageFile, 'ok');
                break;
            case 'bmp':
                imagebmp($image, $imageFile);
                imagedestroy($image);
                $this->log('  Fix image: ' . $imageFile, 'ok');
                break;
        }
    }

}
