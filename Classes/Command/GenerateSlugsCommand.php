<?php
namespace CodingMs\Commands\Command;

use Exception;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\DataHandling\SlugHelper;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class GenerateSlugsCommand extends DefaultCommand
{

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this->setDescription('Generates slugs for a given table.');
        $this->setHelp(
            'Needs three parameters:' . LF
            . 'table: table for which the slugs should be generated, eg. "tt_address"' . LF
            . '* slugField: slug column in the table, eg. "slug"' . LF
            . '* generateFromFields: comma-separated list of fields to generate the slug from, eg. "last_name,uid"' . LF
            . '* prependSlash [optional]: set it to "yes" to prepend a slash, set to "no" to skip this (default "yes") ' . LF
        );
        $this->addArgument('table', InputArgument::REQUIRED, 'Table for which the slugs should be generated, eg. tt_address');
        $this->addArgument('slugField', InputArgument::REQUIRED, 'Slug column in the table, eg. slug');
        $this->addArgument('generateFromFields', InputArgument::REQUIRED, 'Comma-separated list of fields to generate the slug from, eg. last_name,uid');
        $this->addArgument('prependSlash', InputArgument::OPTIONAL, 'PrependSlash (optional, "yes" or "no", default "yes")');
    }

    /**
     * Executes the command for showing sys_log entries
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io->title($this->getDescription());

        $table = $input->getArgument('table');
        $slugField = $input->getArgument('slugField');
        $generateFromFields = $input->getArgument('generateFromFields');
        $prependSlash = strtolower(
            $input->getArgument('prependSlash') ?
                $input->getArgument('prependSlash')
                : 'yes'
        );
        if ($prependSlash !== 'no') {
            $prependSlash = 'yes';
        }

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);
        $records = $queryBuilder
            ->select('*')
            ->from($table)
            ->execute()
            ->fetchAll();

        if (is_array($records)) {
            $this->io->writeln('Migrating slugs for all ' . count($records) . ' records of ' . $table . '.');
            foreach ($records as $record) {
                $slug = $this->createSlug($record, $table, $slugField, $generateFromFields, $prependSlash);
                $this->updateRecord($table, $record, $slugField, $slug);
            }
        }

        return self::SUCCESS;
    }

    /**
     * Creates a slug for the given record by using the given comma-separated list of fields.
     *
     * @param $record
     * @param $table
     * @param $slugField
     * @param $generateFromFields
     * @param $prependSlash
     * @return string
     * @throws Exception
     */
    public function createSlug($record, $table, $slugField, $generateFromFields, $prependSlash) {
        if ($prependSlash === 'yes') {
            $slug = '/';
        } else {
            $slug = '';
        }

        $fields = GeneralUtility::trimExplode(',', $generateFromFields);
        $fieldConfig = [
            'generatorOptions' => [
                'fields' => $fields,
                'fieldSeparator' => '-',
                'replacements' => [
                    '/' => '-'
                ],
                'fallbackCharacter' => '-',
                'prependSlash' => ($prependSlash == 'yes' ? true : false)
            ]
        ];

        /** @var SlugHelper $slugHelper */
        $slugHelper = GeneralUtility::makeInstance(SlugHelper::class, $table, $slugField, $fieldConfig);
        $slug .= $slugHelper->generate($record, $record['pid']);
        return $slug;
    }

    /**
     * Updates the given record and sets the new slug.
     *
     * @param $table
     * @param $record
     * @param $slugField
     * @param $slugValue
     */
    public function updateRecord($table, $record, $slugField, $slugValue) {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);
        $queryBuilder->update($table)
            ->where(
                $queryBuilder->expr()->eq(
                    'uid',
                    $queryBuilder->createNamedParameter($record['uid'], \PDO::PARAM_INT)
                )
            )
            ->set($slugField, $slugValue)
            ->execute();
    }
}
