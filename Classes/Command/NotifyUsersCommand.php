<?php

namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Jona Heidsick <jheidsick@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Exception;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mime\Part\MessagePart;
use Symfony\Component\Mime\RawMessage;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Validation\Validator\EmailAddressValidator;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * User login notification commands
 *
 * @package commands
 */
class NotifyUsersCommand extends DefaultCommand
{
    /**
     * @var EmailAddressValidator
     */
    protected $emailAddressValidator;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \TYPO3\CMS\Extbase\Object\Exception
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);
        $this->emailAddressValidator = $this->objectManager->get(EmailAddressValidator::class);
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Notify users commands from the command line.');
        $this->addArgument('daysNoLogin', InputArgument::OPTIONAL, 'Minimum amount of days that a user has not been logged in', 10);
        $this->addArgument('numberOfMails', InputArgument::OPTIONAL, 'Maximum amount of users to be notified at one execution', 10);
        $this->addArgument('sender', InputArgument::OPTIONAL, 'Sender email address of the emails', 'mail@domain.com');
        $this->addArgument('emailTemplatePath', InputArgument::OPTIONAL, 'Path of the email Fluid template', 'EXT:commands/Resources/Private/Templates/Email/LoginNotification.html');
        $this->addArgument('bcc', InputArgument::OPTIONAL, 'BCC email address', '');
        $this->addArgument('group', InputArgument::OPTIONAL, 'User group filter for the users. If empty this filter will be ignored. If a single number is set, it will be handled as find_in_set. If content is in quotes (for example "1,8") it searches for exactly this string.', '');
        $this->addOption('dry-run', null, InputOption::VALUE_NONE, 'If set the mails will only be sent to bbc address and no changes on frontend user will be performed (mark as notification process e.g.)');
    }

    /**
     * Notifies users to login
     *
     * Notifies users by email to login if they have not been logged in for some time.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $daysNoLogin = $input->getArgument('daysNoLogin');
        $numberOfMails = $input->getArgument('numberOfMails');
        $sender = $input->getArgument('sender');
        $emailTemplatePath = $input->getArgument('emailTemplatePath');
        $bcc = $input->getArgument('bcc');
        $group = $input->getArgument('group');
        $dryRun = $input->getOption('dry-run');

        $notificationsSent = 0;
        try {
            // initialize command
            if (!$this->initializeFileLogging()) {
                return 1;
            }
            if($dryRun) {
                $this->log('!!! DRY RUN !!!', 'warn');
            }
            // get all users from database that have not been logged in for given number of days
            $users = $this->selectUsers($daysNoLogin, $numberOfMails, $group);
            foreach ($users as $user) {
                $this->log('  Last login time ' . date('Y-m-d H:i:s', $user['lastlogin']), 'info');
                $daysSinceLogin = (int)((time() - $user['lastlogin']) / 86400);
                if($this->validateEmail($user['email'])) {
                    $this->log('  Sending reminder to ' . $user['name'] . ' (' . $user['email'] . ') who has not been logged in for ' . $daysSinceLogin . ' days.', 'info');
                    // build email
                    $emailData = $this->renderEmail($emailTemplatePath, $user);
                    $this->log('  Betreff: ' . $emailData['subject'], 'info');
                    // send email to user
                    if (!$this->sendEmail($emailData, $sender, [$user['email'] => $user['name']], $bcc, $dryRun)) {
                        $this->log('  Send reminder email to ' . $user['name'] . ' - ' . $user['email'] . ' failed!', 'error');
                    } else {
                        $notificationsSent++;
                        // set fields in db
                        $this->updateUser($user, $emailData, $dryRun);
                    }
                }
                else {
                    $this->log('  Cant send reminder to ' . $user['name'] . ' (' . $user['email'] . ') who has not been logged in for ' . $daysSinceLogin . ' days, because the email address is invalid!', 'error');
                }
            }
        } catch (Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
        }
        $this->log('Notified ' . $notificationsSent . ' Users', 'info');

        return 0;
    }

    /**
     * @param int $daysNoLogin
     * @param int $maxUsers
     * @param string $group
     * @return array
     * @throws \TYPO3\CMS\Core\Exception
     */
    private function selectUsers(int $daysNoLogin, int $maxUsers, string $group=''): array
    {
        $this->log('  Selecting a maximum of ' . $maxUsers . ' Users that have not logged in for at least ' . $daysNoLogin . ' days.', 'info');
        $limit = time() - 86400 * $daysNoLogin;
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('fe_users');
        $queryBuilder->select('*')
            ->from('fe_users')
            ->where(
                // - disabled != 0
                $queryBuilder->expr()->eq('disable', 0)
            )
            ->andWhere(
                $queryBuilder->expr()->eq('deleted', 0)
            )
            ->andWhere(
                // - no mail sent yet
                $queryBuilder->expr()->eq('tx_commands_last_mail_sent', 0)
            )
            ->andWhere(
                // - today - lastlogin > daysNoLogin
                $queryBuilder->expr()->lt('lastlogin', $limit)
            )
            ->setMaxResults($maxUsers);
        //
        // If group is set
        if(ctype_digit($group) && trim($group) !== '') {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->inSet('usergroup', $group)
            );
        }
        if(!ctype_digit($group) && trim($group) !== '') {
            $group = str_replace('"', '', $group);
            $queryBuilder->andWhere(
                $queryBuilder->expr()->eq(
                    'usergroup',
                    $queryBuilder->createNamedParameter($group, \PDO::PARAM_STR)
                )
            );
        }
        $this->log('  SQL: ' . $queryBuilder->getSQL(), 'info');
        $resource = $queryBuilder->execute();
        $this->log('  Selected ' . $resource->rowCount() . ' Users', 'info');
        return $resource->fetchAll();
    }

    /**
     * @param $user
     * @param $emailData
     * @param boolean $dryRun
     * @throws \TYPO3\CMS\Core\Exception
     */
    private function updateUser($user, $emailData, $dryRun)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('fe_users');
        $queryBuilder->update('fe_users')
            ->where(
                $queryBuilder->expr()->eq('uid', $user['uid'])
            )
            ->set('tx_commands_last_mail_sent', time());
        $this->log('  SQL: ' . $queryBuilder->getSQL(), 'log');
        if(!$dryRun) {
            $queryBuilder->execute();
        }
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('fe_users');
        $queryBuilder->update('fe_users')
            ->where(
                $queryBuilder->expr()->eq('uid', $user['uid'])
            )
            ->set('tx_commands_last_mail_content', serialize($emailData));
        $this->log('  SQL: ' . $queryBuilder->getSQL(), 'log');
        if(!$dryRun) {
            $queryBuilder->execute();
        }
    }

    /**
     * @param $template
     * @param array $user
     * @return array
     */
    protected function renderEmail($template, array $user = []): array
    {
        $emailData = [];
        try {
            /** @var StandaloneView $windowView */
            $emailView = $this->objectManager->get(StandaloneView::class);
            $emailView->setTemplatePathAndFilename($template);
            $emailView->assign('user', $user);
            // Subject rendering
            $emailView->assign('part', 'subject');
            $emailData['subject'] = $emailView->render();
            // Message rendering
            $emailView->assign('part', 'message');
            $emailData['message'] = $emailView->render();
        } catch (Exception $e) {
            $emailData['subject'] = 'Ups.. an error occurred: ' . $e->getMessage();
            $emailData['message'] = $emailData['subject'];
        }
        return $emailData;
    }

    /**
     * @param string $email
     * @return bool|string
     */
    protected function validateEmail($email = '')
    {
        $validateEmailResult = $this->emailAddressValidator->validate($email);
        if (trim($email) === '' || $validateEmailResult->hasErrors()) {
            $email = false;
        }
        return $email;
    }

    /**
     * @param $emailData
     * @param $sender
     * @param $to
     * @param $emailBcc
     * @param boolean $dryRun
     * @return bool
     */
    protected function sendEmail($emailData, $sender, $to, $emailBcc, bool $dryRun): bool
    {
        /** @var MailMessage $mail */
        $mail = GeneralUtility::makeInstance(MailMessage::class);
        $mail->setFrom($sender);
        if($dryRun) {
            $mail->setTo($emailBcc);
        }
        else {
            $mail->setTo($to);
            $emailBcc = $this->validateEmail($emailBcc);
            if ($emailBcc !== false) {
                $mail->setBcc($emailBcc);
            }
        }
        // Subject and message
        if (isset($emailData['subject'])) {
            $mail->setSubject($emailData['subject']);
        }
        if (isset($emailData['message'])) {
            $mail->setBody(new MessagePart(
                new RawMessage($emailData['message'])
            ));
        }
        return (bool)$mail->send();
    }
}
