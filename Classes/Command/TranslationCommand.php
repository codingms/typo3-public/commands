<?php

namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use DOMDocument;
use DOMElement;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Exception;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Translation commands
 *
 * @package commands
 *
 */
class TranslationCommand extends DefaultCommand
{
    /**
     * @var array
     */
    protected array $xliffData = [];

    /**
     * @var array
     */
    protected array $compare = [];

    /**
     * @var array
     */
    protected array $messages = [];

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Translation commands from the command line.');
        $this->addArgument('extensionKey', InputArgument::REQUIRED, 'Extension key (commands)');
        $this->addArgument('languages', InputArgument::OPTIONAL, "Languages comma-separated ('de,en')", 'de,en');
    }

    /**
     * Check extension translations
     *
     * Finds missing translations in extensions.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $extensionKey = $input->getArgument('extensionKey');
        $languages = $input->getArgument('languages');

        $this->executionStartTime = microtime(true);
        //
        $languages = GeneralUtility::trimExplode(',', $languages, true);
        $processedFiles = 0;
        // Catch all exceptions!
        try {
            // initialize command
            if (!$this->initializeFileLogging()) {
                return 1;
            }

            $absFolder = GeneralUtility::getFileAbsFileName('typo3conf/ext/' . $extensionKey . '/');
            $this->log('Extension translation check: ' . $extensionKey, 'info');
            $this->log('Extension translation check: ' . $absFolder, 'info');
            /**
             * @var string $name
             * @var SplFileInfo $object
             */
            $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($absFolder), RecursiveIteratorIterator::SELF_FIRST);
            foreach ($objects as $name => $object) {
                if ($object->isFile()) {
                    $fileExtension = trim(strtolower($object->getExtension()));
                    if($fileExtension === 'xlf') {
                        $this->log('  Translation file: ' . $object->getRealPath(), 'info');

                        $doc = new DOMDocument();
                        $doc->load($object->getRealPath());
                        /** @var DOMElement $file */
                        $file = $doc->getElementsByTagName('file')[0];
                        $this->getNodeAttribute($file, 'source-language', 'sourceLanguage');
                        $this->getNodeAttribute($file, 'target-language', 'targetLanguage');
                        $this->getNodeAttribute($file, 'datatype', 'datatype');
                        $this->getNodeAttribute($file, 'original', 'original');
                        $this->getNodeAttribute($file, 'date', 'date');
                        $this->getNodeAttribute($file, 'product-name', 'productName');

                        $sl = trim($file->getAttribute('source-language'));
                        $tl = '';
                        if($file->hasAttribute('target-language')) {
                            $tl = trim($file->getAttribute('target-language'));
                        }

                        $body = $doc->getElementsByTagName('body')[0];
                        $this->sourceLanguage = $this->xliffData['sourceLanguage'];
                        foreach ($body->childNodes as $bodyItem) {
                            switch (get_class($bodyItem)) {
                                case 'DOMElement':
                                    /** @var \DOMElement $bodyItem */
                                    $id = $bodyItem->getAttribute('id');
                                    $preserveSpace = false;
                                    if ($bodyItem->hasAttribute('xml:space')) {
                                        $preserveSpace = (trim($bodyItem->getAttribute('xml:space')) == 'preserve') ? true : false;
                                    }
                                    /** @var \DOMElement $source */
                                    $source = $bodyItem->getElementsByTagName('source')[0];
                                    $nodeValue = $source->nodeValue;

                                    $this->compare[$id]['file'] = $object->getFilename();

                                    if($tl !== '') {
                                        /** @var \DOMElement $target */
                                        $target = $bodyItem->getElementsByTagName('target')[0];
                                        $targetValue = $target->nodeValue;
                                        $this->compare[$id][$tl] = $targetValue;
                                    }
                                    if (!$preserveSpace) {
                                        $nodeValue = trim($nodeValue);
                                    }
                                    $keyParts = explode('.', $id);
                                    $this->xliffData['body']['source'][] = [
                                        'key' => $id,
                                        'value' => $nodeValue,
                                        'group' => $keyParts[0]
                                    ];
                                    if(!isset($this->compare[$id][$sl])) {
                                        $this->compare[$id][$sl] = $nodeValue;
                                    }
                                    else {
                                        if($this->compare[$id][$sl] !== $nodeValue) {
                                            $messages[] = $id . ' is available but different!';
                                        }
                                    }
                                    break;
                            }
                        }
                        $processedFiles++;
                    }
                }
            }
            foreach($this->compare as $key => $values) {
                foreach($languages as $language) {
                    if(!isset($values[$language])) {
                        $this->messages[] = $language . ' ' . $values['file'] . ' ' . $key . ' is missed!';
                    }
                }
            }
        } catch (Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
            $this->log('Exception-Code: ' . $e->getCode(), 'error');
            $this->log('Exception-Line: ' . $e->getLine(), 'error');
            $this->log('Exception-Trace: ' . $e->getTraceAsString(), 'error');
        }
        $executionEndTime = microtime(true);
        $executionTime = $executionEndTime - $this->executionStartTime;
        $this->log('Checked translation files: ' . $processedFiles, 'info');
        $this->log('Execution time: ' . $executionTime, 'info');

        return 0;
    }

    /**
     * @param DOMElement $file
     * @param string $node
     * @param string $name
     */
    protected function getNodeAttribute(DOMElement $file, $node, $name): void
    {
        if ($file->hasAttribute($node)) {
            $this->xliffData[$name] = trim($file->getAttribute($node));
        }
    }
}
