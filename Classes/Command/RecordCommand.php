<?php

namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Exception;

/**
 * Commands on records
 *
 * @package commands
 *
 */
class RecordCommand extends DefaultCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Record commands from the command line.');
        $this->addArgument('tables', InputArgument::REQUIRED, 'One or more tables for processing (multiple tables comma separated)');
        $this->addArgument('type', InputArgument::OPTIONAL, 'Type of selection (deletedDeleted, deletedAllOlderThanDuration)', 'deletedAllOlderThanDuration');
        $this->addArgument('duration', InputArgument::OPTIONAL, 'Duration in days', 14);
        $this->addOption('dry-run', null, InputOption::VALUE_NONE, 'Run in dry mode?');
    }

    /**
     * Delete records
     *
     * Perform delete actions on records.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tables = $input->getArgument('tables');
        $type = $input->getArgument('type');
        $duration = $input->getArgument('duration');
        $dryRun = $input->getOption('dry-run');

        if (!in_array($type, ['deletedDeleted', 'deletedAllOlderThanDuration'])) {
            $this->io->error("Invalid type '$type'. [Allowed: 'deletedDeleted', 'deletedAllOlderThanDuration']");
            return 1;
        }

        if (!is_int($duration)) {
            $this->io->error("Invalid duration '$duration'. [Allowed: integer]");
            return 1;
        }

        $this->executionStartTime = microtime(true);
        // Catch all exceptions!
        try {
            // initialize command
            if (!$this->initializeFileLogging()) {
                return 1;
            }
            //
            $this->log('Tables: ' . $tables, 'info');
            $this->log('Type: ' . $type, 'info');
            $tables = GeneralUtility::trimExplode(',', $tables, true);
            /** @var ConnectionPool $connectionPool */
            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
            //
            switch ($type) {
                case 'deletedDeleted':
                    $this->log('Delete all records with deleted=1', 'info');
                    foreach ($tables as $table) {
                        $queryBuilder = $connectionPool->getQueryBuilderForTable($table);
                        $queryBuilder->getRestrictions()->removeAll();
                        $queryBuilder->delete($table)->where(
                            $queryBuilder->expr()->eq('deleted', '1')
                        );
                        $this->log('SQL: ' . $queryBuilder->getSQL(), 'warning');
                        if (!$dryRun) {
                            $queryBuilder->execute();
                        }
                    }
                    break;
                case 'deletedAllOlderThanDuration':
                    $this->log('Delete all records with crdate older than duration (' . $duration . ')', 'info');
                    $timestamp = time() - ($duration * (60 * 60 * 24));
                    foreach ($tables as $table) {
                        /** @var QueryBuilder $queryBuilder */
                        $queryBuilder = $connectionPool->getQueryBuilderForTable($table);
                        $queryBuilder->getRestrictions()->removeAll();
                        $queryBuilder->delete($table)->where(
                            $queryBuilder->expr()->lt('crdate', $timestamp)
                        );
                        $this->log('SQL: ' . $queryBuilder->getSQL(), 'warning');
                        if (!$dryRun) {
                            $queryBuilder->execute();
                        }
                    }
                    break;
            }
        } catch (Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
            $this->log('Exception-Code: ' . $e->getCode(), 'error');
            $this->log('Exception-Line: ' . $e->getLine(), 'error');
            $this->log('Exception-Trace: ' . $e->getTraceAsString(), 'error');
        }
        $executionEndTime = microtime(true);
        $executionTime = $executionEndTime - $this->executionStartTime;
        $this->log('Execution time: ' . $executionTime, 'info');

        return 0;
    }
}
