<?php
namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2021 Christian Bülter <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Commands\Utility\LogUtility;
use Exception;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\Index\Indexer;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\ImageService;

class ResizeImagesCommand extends DefaultCommand
{
    CONST IMAGE_EXTENSIONS = ['jpg','jpeg','png'];
    protected int $maxWidth = 0;
    protected int $maxHeight = 0;
    protected int $numberToProcess = 0;
    protected bool $forceOriginalFileName = false;

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure(): void
    {
        defined('LF') ?: define('LF', chr(10));
        $this->setDescription('Resizes images in a given folder to a given maximum width or height.');
        $this->setHelp(
            'Available options:' . LF
            . '* path: path where here images are stored inside the storage, e. g. "my/images/" (non-recursive)' . LF
            . '* storageUid: UID of the storage, e. g. "2" (in most cases this is not needed because 1 this is the default fileadmin storage), default: 1' . LF
            . '* maxWidth: the maximum width the images should be resized to, e. g. "100", optional, default: 200' . LF
            . '* maxHeight: the maximum height the images should be resized to, e. g. "100", optional, default: 200'
            . '* number: the number of images to process, e. g. "50", optional, default: 20'
            . '* forceOriginalFileName: Force original file name, even if it has e. g. brackets or umlauts, set to "1" for true, default: 0 (false)'
        );
        $this->addOption('path', 'p', InputOption::VALUE_REQUIRED, 'Path to images, e. g. "my/images/"');
        $this->addOption('storageUid', 's', InputOption::VALUE_REQUIRED, 'Storage UID, e. g. "1", default: 1', 1);
        $this->addOption('maxWidth', 'w', InputOption::VALUE_OPTIONAL,'Maximum width the images should be resized to, e. g. "100", default: 200', 200);
        $this->addOption('maxHeight', 't', InputOption::VALUE_OPTIONAL,'Maximum height the images should be resized to, e. g. "100", default: 200', 200);
        $this->addOption('number', 'r', InputOption::VALUE_OPTIONAL,'Number of images to process, e. g. "50", default: 20', 20);
        $this->addOption('forceOriginalFileName', 'o', InputOption::VALUE_OPTIONAL,'Force original file name, even if it has e. g. brackets or umlauts, set to "1" for true, default: 0 (false)', false);
    }

    /**
     * Executes the command for resizing images
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getOption('path');
        $storageUid = (int)$input->getOption('storageUid');
        $this->maxWidth = (int)$input->getOption('maxWidth');
        $this->maxHeight = (int)$input->getOption('maxHeight');
        $this->numberToProcess = (int)$input->getOption('number');
        $this->forceOriginalFileName = (bool)$input->getOption('forceOriginalFileName');

        /** @var ResourceFactory $resourceFactory */
        $resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
        $storage = $resourceFactory->getStorageObject($storageUid);
        $folder = $storage->getFolder($path);
        if ($folder instanceof Folder) {
            $this->processFiles($folder);
        }
        return self::SUCCESS;
    }

    /**
     * @param Folder $folder
     */
    public function processFiles(Folder $folder)
    {
        /** @var Indexer $indexer */
        $indexer = GeneralUtility::makeInstance(Indexer::class, $folder->getStorage());
        $files = $folder->getFiles();
        $count = 1;
        foreach ($files as $file) {
            if (in_array($file->getExtension(), self::IMAGE_EXTENSIONS)) {
                // Make sure the meta information is updated in the database.
                $indexer->updateIndexEntry($file);
                $metaData = $file->getMetaData()->get();
                if (
                    $metaData['width'] !== 0 && $metaData['height'] !== 0
                    && ($metaData['height'] > $this->maxHeight || $metaData['width'] > $this->maxWidth)
                ) {
                    $logMessage = 'Scaling ' . $file->getIdentifier() . ' from ' . $metaData['width'] . 'x' . $metaData['height'];
                    $processedFile = $this->scaleImage($file);
                    $newFile = $processedFile->copyTo(
                        $file->getParentFolder(),
                        $file->getName(),
                        DuplicationBehavior::REPLACE
                    );

                    $oldName = $this->getFullFilePath($file);
                    $newName = $this->getFullFilePath($newFile);
                    if ($oldName !== $newName && $this->forceOriginalFileName) {
                        unlink($oldName);
                        copy($newName, $oldName);
                        $newFile->delete();
                    }

                    $indexer->updateIndexEntry($file);
                    $newMetaData = $file->getMetaData()->get();
                    $processedFile->delete();

                    $logMessage .= ' to ' . $newMetaData['width'] . 'x' . $newMetaData['height'];
                    $this->logUtility->log($logMessage, LogLevel::INFO);

                    $count++;
                    if ($count > $this->numberToProcess) {
                        return;
                    }
                }
            }
        }
    }

    /**
     * @param File $file
     * @return ProcessedFile
     */
    public function scaleImage(File $file): ProcessedFile
    {
        $processingInstructions = [
            'maxWidth' => $this->maxWidth,
            'maxHeight' => $this->maxHeight,
            'crop' => null,
        ];

        // Remove the "DeferredBackendImageProcessor" to make sure image is processed directly,
        // not only when the imageUri is called by a browser
        // see https://stackoverflow.com/questions/66131876/typo3-v10-image-processing-in-backend-environment
        $processorConfiguration = $GLOBALS['TYPO3_CONF_VARS']['SYS']['fal']['processors'];
        unset($GLOBALS['TYPO3_CONF_VARS']['SYS']['fal']['processors']['DeferredBackendImageProcessor']);
        $processedImage = $file->process(ProcessedFile::CONTEXT_IMAGECROPSCALEMASK, $processingInstructions);
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['fal']['processors'] = $processorConfiguration;
        return $processedImage;
    }

    /**
     * @param File $file
     * @return string
     */
    public function getFullFilePath(File $file): string
    {
        $storageConfiguration = $file->getStorage()->getConfiguration();
        $storageBasePath = rtrim($storageConfiguration['basePath'] , '/');
        return
            Environment::getPublicPath() . '/'
            . $storageBasePath
            . $file->getIdentifier();
    }
}
