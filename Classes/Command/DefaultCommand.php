<?php

namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2022 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Commands\Utility\LogUtility;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Core\Bootstrap;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageQueue;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Mvc\Controller\CommandController;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * DefaultCommand
 */
class DefaultCommand extends Command
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * @var LogUtility
     */
    protected $logUtility;

    /**
     * @var bool
     */
    protected bool $initializeBackendAuthentication = false;

    /**
     * @var float
     */
    protected float $executionStartTime = 0.0;

    /**
     * @var FlashMessageService $flashMessageService
     */
    private $flashMessageService = null;

    /**
     * @var FlashMessageQueue $defaultFlashMessageQueue
     */
    private $defaultFlashMessageQueue = null;

    /**
     * Base folder
     * @var string
     */
    protected string $baseFolder = '';

    /**
     * @var string
     */
    protected string $logFilename = '';

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var SymfonyStyle $io */
        $io = GeneralUtility::makeInstance(SymfonyStyle::class, $input, $output);

        $this->objectManager = $objectManager;
        $this->io = $io;

        $this->logUtility = GeneralUtility::makeInstance(LogUtility::class, static::class, $this->io);

        if ($this->initializeBackendAuthentication) {
            Bootstrap::initializeBackendAuthentication();
        }
    }

    /**
     * @return bool
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function initializeFileLogging(): bool
    {
        $success = true;

        $classNameParts = explode('\\', get_class($this));
        $className = array_pop($classNameParts);

        // Create filename for log file
        $logFilenameExtension = GeneralUtility::camelCaseToLowerCaseUnderscored($className);
        $logFilenameExtension = str_replace('_', '-', $logFilenameExtension);
        $this->logFilename = date('Y-m-d_H-i-s') . '_command-' . $logFilenameExtension . '.log';

        // Get base folder
        $this->baseFolder = GeneralUtility::getFileAbsFileName('typo3temp/tx_commands/');
        if (!file_exists($this->baseFolder) && $success) {
            $success = GeneralUtility::mkdir($this->baseFolder);
        }

        // Create required directories
        $logDir = GeneralUtility::getFileAbsFileName($this->baseFolder . 'log/');
        if (!file_exists($logDir) && $success) {
            $success = GeneralUtility::mkdir($logDir);
            $this->log("Create folder: '" . $logDir . "'", 'info');
        }

        // .htaccess file check
        if (!file_exists($logDir . '.htaccess') && $success) {
            $htaccess = 'deny from all';
            $success = file_put_contents($logDir . '.htaccess', $htaccess);
        }

        return (bool)$success;
    }

    /**
     * Log some messages
     *
     * @param string $message Message text
     * @param int|string $severity Severity: 0 is info, 1 is notice, 2 is warning, 3 is fatal error, -1 is "OK" message
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function log(string $message, $severity = 0)
    {
        if (!defined('TYPO3_DLOG')) {
            define('TYPO3_DLOG', 0);
        }

        switch ($severity) {
            case 0:
            case 'info':
                $loggerMethodName = 'info';
                break;
            case 1:
            case 'notice':
                $loggerMethodName = 'note';
                break;
            case 2:
            case 'warning':
                $loggerMethodName = 'warning';
                break;
            case 3:
            case 'error':
                $loggerMethodName = 'error';
                break;
            case -1:
            case 'ok':
                $loggerMethodName = 'success';
                break;
            default:
                $loggerMethodName = 'writeln';
                break;
        }

        $message .= ' (Time: ' . (microtime(true) - $this->executionStartTime) . ')';
        if (TYPO3_DLOG && $severity != 'log') {
            $severityId = 2;
            switch ($severity) {
                case 'info':
                    $severityId = 0;
                    $loggerMethodName = 'info';
                    break;
                case 'notice':
                    $severityId = 1;
                    $loggerMethodName = 'note';
                    break;
                case 'warning':
                    $severityId = 2;
                    $loggerMethodName = 'warning';
                    break;
                case 'error':
                    $severityId = 3;
                    $loggerMethodName = 'error';
                    break;
                case 'ok':
                    $severityId = -1;
                    $loggerMethodName = 'success';
                    break;
            }
            GeneralUtility::devLog($message, 'commands', $severityId);
        }
        // Executed by scheduler?!
        if (PHP_SAPI != 'cli' && $severity != 'log') {
            if ($this->flashMessageService === null) {
                $this->flashMessageService = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Messaging\\FlashMessageService');
            }

            if ($this->defaultFlashMessageQueue === null) {
                $this->defaultFlashMessageQueue = $this->flashMessageService->getMessageQueueByIdentifier();
            }

            /**
             * Severity classes
             * self::NOTICE => 'notice',
             * self::INFO => 'info',
             * self::OK => 'success',
             * self::WARNING => 'warning',
             * self::ERROR => 'danger'
             */
            $severityClass = FlashMessage::WARNING;

            switch ($severity) {
                case 'notice':
                    $severityClass = FlashMessage::NOTICE;
                    break;
                case 'info':
                    $severityClass = FlashMessage::INFO;
                    break;
                case 'ok':
                    $severityClass = FlashMessage::OK;
                    break;
                case 'warning':
                    $severityClass = FlashMessage::WARNING;
                    break;
                case 'error':
                    $severityClass = FlashMessage::ERROR;
                    break;
            }

            /** @var FlashMessage $flashMessageInfo */
            $flashMessageInfo = GeneralUtility::makeInstance(FlashMessage::class, $message, '', $severityClass);
            $this->defaultFlashMessageQueue->enqueue($flashMessageInfo);
        } else {
            if ($severity != 'log') {
                $this->io->{$loggerMethodName}($message);
            }
        }
        // Always push to log file
        file_put_contents($this->baseFolder . 'log/' . $this->logFilename, $message . "\n", FILE_APPEND);
    }

    final protected function executionTimeStart(): void
    {

    }
}

