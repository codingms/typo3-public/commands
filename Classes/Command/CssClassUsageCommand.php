<?php

namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use PDO;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Exception;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Summarizes all used CSS classes in tt_content bodytext.
 *
 * @package commands
 *
 */
class CssClassUsageCommand extends DefaultCommand
{
    /**
     * @var array
     */
    protected $folder = [];

    /**
     * CSS class usage analyzer (experimental!).
     * Summarizes all used CSS classes in tt_content bodytext.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->executionStartTime = microtime(true);
        $cssClasses = [
            'all' => [],
        ];
        // Catch all exceptions!
        try {
            // initialize command
            if (!$this->initializeFileLogging()) {
                return 1;
            }
            /** @var ConnectionPool $connectionPool */
            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = $connectionPool->getQueryBuilderForTable('tt_content');
            $queryBuilder->select('*')
                ->from('tt_content')
                ->where(
                    $queryBuilder->expr()->neq('bodytext', '""')
                )
                ->andWhere(
                    $queryBuilder->expr()->neq('CType', '"html"')
                )
                ->andWhere(
                    $queryBuilder->expr()->eq('hidden', '0')
                )
                ->andWhere(
                    $queryBuilder->expr()->eq('deleted', '0')
                );
            $resource = $queryBuilder->execute();
            while ($row = $resource->fetch(PDO::FETCH_ASSOC)) {
                $classParts = explode('class="', $row['bodytext']);
                if(count($classParts) > 1) {
                    for($i=1 ; $i<count($classParts) ; $i++) {
                        $classPart = explode('"', $classParts[$i]);
                        $classes = GeneralUtility::trimExplode(' ', $classPart[0], true);
                        foreach($classes as $class) {
                            if(!isset($cssClasses['all'][$class])) {
                                $cssClasses['all'][$class] = [];
                            }
                            $cssClasses['all'][$class][$row['CType']][] = $row['uid'];
                            $cssClasses['all'][$class][$row['CType']] = array_unique($cssClasses['all'][$class][$row['CType']]);
                        }
                    }
                }
            }
            DebuggerUtility::var_dump($cssClasses);
        } catch (Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
            $this->log('Exception-Code: ' . $e->getCode(), 'error');
            $this->log('Exception-Line: ' . $e->getLine(), 'error');
            $this->log('Exception-Trace: ' . $e->getTraceAsString(), 'error');
        }
        $executionEndTime = microtime(true);
        $executionTime = $executionEndTime - $this->executionStartTime;
        $this->log('Execution time: ' . $executionTime, 'info');

        return 0;
    }
}
