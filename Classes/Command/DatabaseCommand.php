<?php
namespace CodingMs\Commands\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Exception;
use RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\CommandUtility;

/**
 * Database commands
 *
 * @package commands
 *
 */
class DatabaseCommand extends DefaultCommand
{
    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setDescription('Database commands from the command line.');
    }

    /**
     * @return bool
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function initializeFileLogging(): bool
    {
        $success = parent::initializeFileLogging();

        // Create required directories
        $exportDir = GeneralUtility::getFileAbsFileName($this->baseFolder . 'export/');
        if (!file_exists($exportDir) && $success) {
            $success = GeneralUtility::mkdir($exportDir);
            $this->log("Create folder: '" . $exportDir . "'", 'info');
        }

        // .htaccess file check
        if (!file_exists($exportDir . '.htaccess') && $success) {
            $htaccess = 'deny from all';
            $success = (bool)file_put_contents($exportDir . '.htaccess', $htaccess);
        }

        return $success;
    }

    /**
     * Exports the database.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \TYPO3\CMS\Core\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ignoreTables = 'sys_log,sys_history,tx_extensionmanager_domain_model_extension';
        $this->executionStartTime = microtime(true);
        // Catch all exceptions!
        try {
            // initialize command
            if (!$this->initializeFileLogging()) {
                return 1;
            }
            // Extract ignore-tables string
            $ignoreTablesArray = GeneralUtility::trimExplode(',', $ignoreTables, true);
            $this->log('Passed folder: ' . implode(',', $ignoreTablesArray), 'info');
            //
            $userBin = 'mysqldump';
            $absUserBin = CommandUtility::getCommand(escapeshellcmd($userBin));
            if (!is_string($absUserBin)) {
                throw new RuntimeException('Binary ' . $absUserBin . ' not found', 1488631746);
            } else {
                $this->log('/usr/bin/' . $absUserBin . ' found!', 'ok');
            }
            // Create filename for log file
            $exportFilename = date('Y-m-d_H-i-s') . '_db-export.sql';
            $exportFolder = GeneralUtility::getFileAbsFileName('typo3temp/tx_commands/export/');
            $exportFile = $exportFolder . $exportFilename;
            $ignoreTablesString = $this->buildIgnoreTables($ignoreTablesArray);
            $parameters = implode(' ', $this->buildConnectionArguments()) . $ignoreTablesString;
            $shellCommand = $absUserBin . ' ' . $parameters . ' > ' . $exportFile . ' 2>&1';
            $output = array();
            $returnValue = 0;
            //
            // Hide password from log!
            $dbConfig = $this->getDatabaseConnection();
            $commandLog = str_replace($dbConfig['password'], '********', $shellCommand);
            //
            // Execute export command
            $this->log('Execute: ' . $commandLog, 'info');
            CommandUtility::exec($shellCommand, $output, $returnValue);
            $this->log('Output was: ' . implode(' ', $output), 'ok');
        } catch (Exception $e) {
            $this->log('Exception: ' . $e->getMessage(), 'error');
            $this->log('Exception-Code: ' . $e->getCode(), 'error');
            $this->log('Exception-Line: ' . $e->getLine(), 'error');
            $this->log('Exception-Trace: ' . $e->getTraceAsString(), 'error');
        }
        $executionEndTime = microtime(true);
        $executionTime = $executionEndTime - $this->executionStartTime;
        $this->log('Execution time: ' . $executionTime, 'info');

        return 0;
    }

    /**
     * Returns a normalized DB configuration array
     *
     * @return array
     */
    protected function getDatabaseConnection(): array
    {
        if (isset($GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']) && is_array($GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'])) {
            // We are on TYPO3 > 8.0
            $dbConfig = $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'];
        } else {
            // @deprecated with 8 will be removed when 7 Support is dropped
            $dbConfig = [
                'dbname' => $GLOBALS['TYPO3_CONF_VARS']['DB']['database'],
                'host' => $GLOBALS['TYPO3_CONF_VARS']['DB']['host'],
                'user' => $GLOBALS['TYPO3_CONF_VARS']['DB']['username'],
                'password' => $GLOBALS['TYPO3_CONF_VARS']['DB']['password'],
            ];
            if (isset($GLOBALS['TYPO3_CONF_VARS']['DB']['port'])) {
                $dbConfig['port'] = $GLOBALS['TYPO3_CONF_VARS']['DB']['port'];
            }
            if (isset($GLOBALS['TYPO3_CONF_VARS']['DB']['socket'])) {
                $dbConfig['unix_socket'] = $GLOBALS['TYPO3_CONF_VARS']['DB']['socket'];
            }
        }
        return $dbConfig ?? [];
    }

    /**
     * @param $ignoreTablesArray
     * @return string
     */
    protected function buildIgnoreTables($ignoreTablesArray)
    {
        $ignoreTablesString = '';
        $databaseConfiguration = $this->getDatabaseConnection();

        foreach($ignoreTablesArray as $table) {
            $ignoreTablesString .= ' --ignore-table=' . $databaseConfiguration['dbname'] . '.' . $table;
        }

        return $ignoreTablesString;
    }

    /**
     * @return array
     */
    protected function buildConnectionArguments(): array
    {
        $databaseConfiguration = $this->getDatabaseConnection();

        $arguments = [];
        if (!empty($databaseConfiguration['user'])) {
            $arguments[] = '-u';
            $arguments[] = $databaseConfiguration['user'];
        }
        if (!empty($databaseConfiguration['password'])) {
            $arguments[] = '-p' . $databaseConfiguration['password'];
        }
        if (!empty($databaseConfiguration['host'])) {
            $arguments[] = '-h';
            $arguments[] = $databaseConfiguration['host'];
        }
        if (!empty($databaseConfiguration['port'])) {
            $arguments[] = '-P';
            $arguments[] = $databaseConfiguration['port'];
        }
        if (!empty($databaseConfiguration['unix_socket'])) {
            $arguments[] = '-S';
            $arguments[] = $databaseConfiguration['unix_socket'];
        }
        $arguments[] = $databaseConfiguration['dbname'];

        return $arguments;
    }
}
