# Commands Erweiterung für TYPO3

Diese Erweiterung bietet Dir einige nützliche Befehle für den TYPO3-Scheduler und die Konsole.

**Features:**

*   Automatisches Zippen von Ordnern (Shell tar, PHP/PEAR tar)
*   Automatischer Datenbankexport
*   Automatische Bildoptimierung (jpeg, png) durch Verwendung von jpegoptim
*   Automatische Korrektur der Bilddateiendung
*   Fasst alle verwendeten CSS-Klassen in tt_content bodytext zusammen
*   Bereinigung von Datensätzen in der Datenbank
*   Frontend-Benutzer Benachrichtigung die seit x Tagen nicht eingeloggt sind
*   Automatische Slug-Generierung für benutzerdefinierte Datensätze
*   RTE-Bilder nach TYPO3-Upgrade/Migration reparieren
*   Größenänderungen von Bildern in einem Ordner bis zu einer MaximalgrößeÄnderung

Wenn ein zusätzliches oder individuelle Feature benötigt wird - nehme Kontakt auf!

**Links:**

*   [Commands Dokumentation](https://www.coding.ms/documentation/typo3-commands "Commands Dokumentation")
*   [Commands Bug-Tracker](https://gitlab.com/codingms/typo3-public/commands/-/issues "Commands Bug-Tracker")
*   [Commands Repository](https://gitlab.com/codingms/typo3-public/commands "Commands Repository")
*   [TYPO3 Commands](https://www.coding.ms/typo3-extensions/typo3-commands/ "TYPO3 Commands")
