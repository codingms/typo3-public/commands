# Commands Change-Log

*	[TASK] Fix path to documentation readme files



## 2022-05-13  Release of 1.5.2

*	[TASK] Normalize documentation configuration
*	[TASK] Allow execution of GenerateSlugs command in scheduler
*	[BUGFIX] Add missing return value for GenerateSlugs command



## 2022-03-02  Release of 1.5.1

*	[TASK] ResizeImages command: Make sure that images are processed directly, not only when the image URL is called by a browser



## 2022-02-17  Release of 1.5.0
[!!!] This version drops support for TYPO3 9

*	[TASK] Migrate Extbase CommandControllers to Symfony Commands
*	[TASK] Preparation for TYPO3 10+11 and removing support for TYPO3 9
*	[BUGFIX] Fix getting absolute folder for frontend files
*	[TASK] Add documentation configuration
*	[TASK] Optimize documentations configuration



## 2021-12-02  Release of 1.4.1

*	[TASK] Add option forceOriginalFileName for ResizeImages command



## 2021-11-30  Release of 1.4.0

*	[FEATURE] Add command for resizing images



## 2020-08-20  Release of 1.3.1

*	[BUGFIX]  Moved extra tags to the right scope in composer.json



## 2020-08-20  Release of 1.3.0

*	[FEATURE] Add group and dryRun parameter for NotifyUser command
*	[FEATURE] Add command for fix RTE images after TYPO3 upgrade/migration
*	[TASK] Add documentation files for all commands
*	[TASK] Rise PHP version up to 7.2



## 2020-04-14  Release of 1.2.0

*	[FEATURE] Add command for generating slugs



## 2020-04-07  Release of 1.1.0

*	[FEATURE] Add command for delete records
*	[BUGFIX] Fix database command path for mysql dump
*	[BUGFIX] Fix filesystem command path for tar dump
*	[FEATURE] Add command for notifying frontend users to login
*	[FEATURE] Add command for summarizing CSS class usage of tt_content bodytext.
*	[FEATURE] Add command for fixing images with wrong content type or file extension.
*	[TASK] Change export folder from /typo3temp/tx_commands/ to /tx_commands/.
*	[TASK] Hide sensitive data from log files.
*	[TASK] Create a more detailed log filename.
*	[FEATURE] Add command for creating a tar by using Archive_Tar PEAR library.
*	[TASK] Initial import.
