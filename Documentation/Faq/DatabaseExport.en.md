# Database export exits with: mysqldump: Couldn't execute 'SET OPTION SQL_QUOTE_SHOW_CREATE=1'

The reason for this is that MySQL 5.6 has removed support for "SET OPTION" and your mysql client tools are probably on
older version. Most likely 5.5 or 5.1. There is more info about this issue on MySQL bugs website:
http://bugs.mysql.com/bug.php?id=66765

Solution: http://www.markomedia.com.au/mysqldump-mysql-5-6-problem-solved/
