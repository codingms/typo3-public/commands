# Generate slug commands

## Execute command

Generates slugs for a given table. The passed comma separated fields will be *slugified* and afterwards written in slug field.

| Parameter          | Type   | Default | Description                                                                 |
|--------------------|--------|---------|-----------------------------------------------------------------------------|
| table              | string |         | Table for which the slugs should be generated, eg. tt_address               |
| slugField          | string |         | Slug column in the table, eg. slugT                                         |
| generateFromFields | string |         | Comma-separated list of fields to generate the slug from, eg. last_name,uid |
| prependSlash       | string | yes     | PrependSlash (optional, "yes" or "no", default "yes")                       |
