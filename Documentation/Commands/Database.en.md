# Database commands

## Export command

Exports the database into a SQL dump file. The dump file will be stored in `typo3temp/tx_commands/export/`, which will automatically restricted with a `.htaccess` file.

| Parameter    | Type   | Default                                                        | Description                                                    |
|--------------|--------|----------------------------------------------------------------|----------------------------------------------------------------|
| ignoreTables | string | sys_log,sys_history,tx_extensionmanager_domain_model_extension | Comma-separated list of table names to exclude from the export |

