# File system commands

## Tar folder command (by using PHP Archive/Tar)

Create a tart from a folder using Archive_Tar (PEAR library).

| Parameter | Type   | Default                     | Description                                                                |
|-----------|--------|-----------------------------|----------------------------------------------------------------------------|
| folder    | string | fileadmin,typo3conf,uploads | Comma-separated list of folder, for example: fileadmin,uploads/tx_commands |

## Tar folder command (by using shell commands)

Create a tar/zip from a folder.

| Parameter | Type   | Default                     | Description                                                                |
|-----------|--------|-----------------------------|----------------------------------------------------------------------------|
| folder    | string | fileadmin,typo3conf,uploads | Comma-separated list of folder, for example: fileadmin,uploads/tx_commands |

