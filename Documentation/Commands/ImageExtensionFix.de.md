# Image extension fix commands

## Execute command

Finds all images where content type and image file extension is not the same and fix the content. While fixing this issue the file name will be remained, so that there're no broken file references.

| Parameter  | Type   | Default | Description                                     |
|------------|--------|---------|-------------------------------------------------|
| identifier | string |         | Folder identifier, for example 1:/user_content/ |

This command is able to progress the following image types: gif, jpg, jpeg, png, bmp