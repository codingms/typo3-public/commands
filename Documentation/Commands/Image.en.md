# Image commands

## Optimize Jpeg command

Optimizes all JPEG images in _processed_-Folder of all existing storages using `jpegoptim`.

| Parameter | Type | Default | Description                   |
|-----------|------|---------|-------------------------------|
| quality   | int  | 70      | Image quality (0-100 percent) |


## Optimize Png command

Optimizes all PNG images in _processed_-Folder of all existing storages using `optipng`.

| Parameter | Type | Default | Description                   |
|-----------|------|---------|-------------------------------|
| quality   | int  | 70      | Image quality (0-100 percent) |
