# Record commands

## Record delete command

Performs delete action on records.

| Parameter | Type   | Default                     | Description                                     |
|-----------|--------|-----------------------------|-------------------------------------------------|
| tables    | string |                             | Single table or multiple tables comma separated |
| type      | string | deletedAllOlderThanDuration | Described below                                 |
| duration  | int    | 14                          | Duration in days                                |
| dryRun    | bool   | false                       | Displays only SQL queries                       |

### Available execution types:

**deletedAllOlderThanDuration:**
Delete records that are older than the passed amount of days. This calculation is based on `crdate` field.

**deletedDeleted:**
Delete records that have `deleted=1`.