# Resize images command

Resizes images in a given folder to a given maximum width or height.

This is useful e. g. if there are large amounts of images which are not needed in a high resolution and disk space should be saved.


| Parameter          | Type   | Default | Description                                                               |
|--------------------|--------|---------|---------------------------------------------------------------------------|
| storageUid              | int | 1       | UID of the storage, mostly fileadmin/ with UID 1.                         |
| path          | string |         | path where the images are stored inside the given storage (non-recursive) |
| maxWidth | int | 200     | maximum width the images should be resized to                             |
| maxHeight       | int | 200     | maximum height the images should be resized to                            |
| number       | int | 20      | number of images to process per run                                       |

## Troubleshooting

### Files with Umlauts don`t get resized

Make sure that `systemLocale` and `UTF8filesystem` is set in the TYPO3 configuration.

```
[SYS][UTF8filesystem] = true
[SYS][systemLocale] = de_DE.UTF-8
```

Start `Remove Temporary Assets` in the `Maintenance` module in the backend after setting this configuration.
