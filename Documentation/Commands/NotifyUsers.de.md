# Notify users commands

## Execute command

Notifies users by email to login if they have not been logged in for some time.

| Parameter         | Type    | Default                                                               | Description                                                             |
|-------------------|---------|-----------------------------------------------------------------------|-------------------------------------------------------------------------|
| daysNoLogin       | int     | 10                                                                    | Minimum amount of days that a user has not been logged in               |
| numberOfMails     | int     | 10                                                                    | Maximum amount of users to be notified at one execution                 |
| sender            | string  | email@example.com                                                     | Sender email address of the emails                                      |
| emailTemplatePath | string  | EXT:commands/Resources/Private/Templates/Email/LoginNotification.html | Path of the email Fluid template                                        |
| bcc               | string  |                                                                       | BCC email address                                                       |
| group             | int     | 0                                                                     | User group filter for the users. If empty this filter will be ignored. If a single number is set, it will be handled as find_in_set. If content is in quotes (for example "1,8") it searches for exactly this string. |
| dryRun            | boolean | false                                                                 | If set the mails will only be sent to bbc address and no changes on frontend user will be performed (mark as notification process e.g.) |

On execution the user field `tx_commands_last_mail_sent` will be set to `1`, because we need to know which users receives already our notification mail. When you need to send an additional notification afterwards, you need to reset this flag.