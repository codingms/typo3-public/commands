# Image RTE fix commands

## Fix migrated images command

**Situation:**
You performed an TYPO3 Upgrade and the instance uses image in RTE context. Before starting the upgrade the images are located in `uploads/RTEmagicC_S-37-Sattelhalter-1.jpg`, afterwards they are migrated in `fileadmin/_migrated/RTE/RTEmagicC_S-37-Sattelhalter-2.jpg`.
In our case the migrated files were resized down and we need to override the migrated files with the orginal ones.



| Parameter | Type   | Default                     | Description                                     |
|-----------|--------|-----------------------------|-------------------------------------------------|
| tables    | string |                             | Single table or multiple tables comma separated |
| type      | string | deletedAllOlderThanDuration | Described below                                 |
| duration  | int    | 14                          | Duration in days                                |
| dryRun    | bool   | false                       | Displays only SQL queries                       |

### Available execution types:

**deletedAllOlderThanDuration:**
Delete records that are older than the passed amount of days. This calculation is based on `crdate` field.

**deletedDeleted:**
Delete records that have `deleted=1`.