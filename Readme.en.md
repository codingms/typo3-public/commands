# Commands Extension for TYPO3

This extension provides you some useful commands for the TYPO3 scheduler and console.

**Features:**

*   Automatic folder zipping (shell tar, PHP/PEAR tar)
*   Automatic database export
*   Automatic image optimization (jpeg, png) by using jpegoptim
*   Automatic image extension fix. Finds all images where content type and image file extension doesn't equals and fix the content
*   Summarize all used CSS classes in tt_content bodytext. Useful for preparing migration of RTE
*   Cleaning up records in database
*   Notify frontend user which are not loggedin for x days
*   Automatic slug generation for custom records
*   Fix RTE images after TYPO3 upgrade/migration
*   Resize images in a given folder to a given maximum width and height

If you need some additional or custom feature - get in contact!

**Links:**

*   [Commands Documentation](https://www.coding.ms/documentation/typo3-commands "Commands Documentation")
*   [Commands Bug-Tracker](https://gitlab.com/codingms/typo3-public/commands/-/issues "Commands Bug-Tracker")
*   [Commands Repository](https://gitlab.com/codingms/typo3-public/commands "Commands Repository")
*   [TYPO3 Commands](https://www.coding.ms/typo3-extensions/typo3-commands/ "TYPO3 Commands")
