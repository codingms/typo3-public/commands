<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'CodingMs\\Commands\\Command\\DatabaseCommand';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'CodingMs\\Commands\\Command\\FileSystemCommand';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'CodingMs\\Commands\\Command\\ImageCommand';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'CodingMs\\Commands\\Command\\ImageExtensionFixCommand';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'CodingMs\\Commands\\Command\\CssClassUsageCommand';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'CodingMs\\Commands\\Command\\RecordCommand';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'CodingMs\\Commands\\Command\\NotifyUsersCommand';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'CodingMs\\Commands\\Command\\ImageRteFixCommand';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['extbase']['commandControllers'][] = 'CodingMs\\Commands\\Command\\TranslationCommand';

$GLOBALS['TYPO3_CONF_VARS']['LOG']['CodingMs']['Commands']['Command']['writerConfiguration'] = [
    'info' => [
        'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => [
            'logFileInfix' => 'commands'
        ]
    ]
];
